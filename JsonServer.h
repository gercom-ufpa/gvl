/*
 * JsonServer.h
 *
 *  Created on: 29 de nov de 2016
 *      Author: familia
 */
//based on https://github.com/denischatelain/libevent-http-server-get-example/blob/master/libevent-http-server-get-example.c


#ifndef JSONSERVER_H_
#define JSONSERVER_H_

#include <evhttp.h>

class JsonServer {
private:
	struct event_base *ebase;
	struct evhttp *server;
	char* _address = "0.0.0.0";
	int _port = 6653;
	std::vector<Slice> * _slices;


public:
	JsonServer(char* address, int port = 8080);
	int Start();
	void Stop();
	virtual ~JsonServer();
	inline void setSlices(std::vector<Slice> * slices)
	{
		_slices = slices;
	}
};





/**
 *
 * @param *request the opaque data structure containing the request infos
 * @param *privParams global parameters set when the callback was associated
 * @return nothing
 */

void slice (struct evhttp_request *request, void *privParams)
{
	std::vector<Slice> * slices = (std::vector<Slice> *) privParams;



	struct evbuffer *buffer;
	struct evkeyvalq headers;
	const char *id=NULL;
	// Parse the query for later lookups
	evhttp_parse_query (evhttp_request_get_uri (request), &headers);

	// lookup the 'q' GET parameter
	id = evhttp_find_header (&headers, "id");

	// Create an answer buffer where the data to send back to the browser will be appened
	buffer = evbuffer_new ();


	if (!id)
	{
		for (Slice n : *slices)
		{
			std::cout << "teste 2" << std::endl;
			std::string s = n.getJsonString();
			evbuffer_add_printf (buffer, "%s",s.data());
		}
	}
	else
	{
			evbuffer_add_printf (buffer, "%s", id);
	}

	// Add a HTTP header, an application/json for the content type here
	evhttp_add_header (evhttp_request_get_output_headers (request),
			"Content-Type", "text/plain");

	// Tell we're done and data should be sent back
	evhttp_send_reply(request, HTTP_OK, "OK", buffer);

	// Free up stuff
	evhttp_clear_headers (&headers);

	evbuffer_free (buffer);

	return;
}

/**
 *
 * @desc the "404 Not Found" callback; catches any unrecognized path requested
 * @return nothing
 */
void notfound(struct evhttp_request *request, void *params) {
	evhttp_send_error(request, HTTP_NOTFOUND, "Not Found");
}


#endif /* JSONSERVER_H_ */
