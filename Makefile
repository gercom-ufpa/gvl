CC=cc
CXX=c++
RUN_FLAGS=-I. -O3 -std=c++0x
DBG_FLAGS=-I. -g  -std=c++0x

all: msg_controller 
all_for_reals: all 

msg_controller:
	$(CXX) MsgController.cc -lfluid_msg -lfluid_base -lboost_system -levent -lpthread -lrestclient-cpp -lcurl $(RUN_FLAGS) -o msg_controller


# TODO: don't duplicate the code above
msg_controller_debug:
	$(CXX) MsgController.cc -lfluid_msg -lfluid_base -lboost_system -levent -lpthread  -lrestclient-cpp -lcurl $(DBG_FLAGS) -o msg_controller


clean:
	rm -f msg_controller
	rm -f *.gch

.PHONY : msg_controller  clean
