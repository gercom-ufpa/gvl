#!/usr/bin/python

"""
This example creates a multi-controller network from
semi-scratch; note a topo object could also be used and
would be passed into the Mininet() constructor.
"""

from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.cli import CLI
from mininet.log import setLogLevel
import os

Switch = OVSKernelSwitch

def netTeste():
    "Rede com Controlador Remoto e Switches em Malha"

    net = Mininet( controller=RemoteController, switch=Switch)

    print "*** Criando Controlador Remoto"
    c1 = net.addController( 'c1',defaultIP='127.0.0.1' ,port=6634 )


    print "*** Criando os Switches"
    s1 = net.addSwitch( 's1',protocols=["OpenFlow13"] )
    s2 = net.addSwitch( 's2',protocols=["OpenFlow13"] )


    print "*** Criando HOSTS"
    host1 = net.addHost( 'h1', ip='192.168.0.1', mac='00:00:00:00:00:11')
    host2 = net.addHost( 'h2', ip='192.168.0.2', mac='00:00:00:00:00:12')
    host3 = net.addHost( 'h3', ip='192.168.0.1', mac='00:00:00:00:00:13')
    host4 = net.addHost( 'h4', ip='192.168.0.2', mac='00:00:00:00:00:14')

    print "*** Creating links"
    
    s1.linkTo( host1 ) 
    s1.linkTo( host3 ) 

    s1.linkTo( s2 )

    
    s2.linkTo( host2 )
    s2.linkTo( host4 ) 
  
    
    net.build()
    c1.start()
    s1.start( [ c1 ] )
    s2.start( [ c1 ] )


    print "*** Inicializado CLI"
    CLI( net )

    print "*** Parando Rede"
    net.stop()
    os.system("mn -c")

if __name__ == '__main__':
    setLogLevel( 'info' )  
    netTeste()
