/*
 * graph.hh
 *
 *  Created on: 15 de fev de 2016
 *      Author: billy
 */

#ifndef GRAPH_HH_
#define GRAPH_HH_

#include <stdint.h>
#include <iostream>
#include <set>
#include <map>
#include <string>

#include "json.hpp"
//using json = nlohmann::json;

struct vertex{
	typedef std::pair<int,vertex*> ve;
	std::set<ve> adj; //cost of edge, destination vertex
	uint64_t id;
	vertex(uint64_t _id)
	{
		id=_id;
	}
};

class Graph
{
	public:
		typedef std::map<uint64_t, vertex *> vmap;
		nlohmann::json j;
		vmap work;
		void addvertex(uint64_t id);
		void addedge(uint64_t from, uint64_t to, double cost);
		void show();
		std::string json();
};

// nao esta terminado isso pois ainda nao foi definido como adicionar a porta ao link, tbm falta seguir a notação usada no gnc
/*
      "links":[
        {
            "linkId":14.0,
            "dst":{
                "port":"1",
                "dpid":"00:00:00:00:00:00:00:05"
            },
            "src":{
                "port":"2",
                "dpid":"00:00:00:00:00:00:00:04"
            }
        },
 *
 * */
void Graph::addvertex(uint64_t id)
{
	vmap::iterator itr=work.begin();
	itr=work.find(id);
	if(itr==work.end())
	{
		vertex *v;
		v= new vertex(id);
		work[id]=v;
		return;
	}
	std::cout<<"\nVertex already exists!\n";
}

void Graph::addedge(uint64_t from, uint64_t to, double cost)
{
	vertex *f=(work.find(from)->second);
	vertex *t=(work.find(to)->second);
	std::pair<int,vertex *> edge = std::make_pair(cost,t);
	f->adj.insert(edge);
}

void Graph::show()
{

	std::cout << "\n ##### Grafo Nodes ##### ";
	for(auto const &nodes:  this->work)
	{
		std::cout << "\n Node " << nodes.first << " :  ";
		for( auto const &links: nodes.second->adj)
		{
			std::cout << links.first << " - "<< std::hex << links.second->id << " | " ;

		}
		std::cout << "\n Morreu esse no\n "  ;
	}
	std::cout << std::endl;

}

//iniciei mas ele só pode ser terminando, inclusive testado, quando existir de fato um grafo para ser exporto em json.
//Precisam fazer a inserção do l2table no grafo primeiro.

std::string Graph::json()
{
	std::cout << "\n ##### Grafo Nodes ##### ";
	for(auto const &nodes:  this->work)
	{
		std::cout << "\n Node " << nodes.first << " :  ";
		//adicionado o switch
		j["switches"].push_back(nodes.first);

		for( auto const &links: nodes.second->adj)
		{
			std::cout << links.first << " - "<< std::hex << links.second->id << " | " ;
			//deve adicionar os links.

		}
		std::cout << "\n Morreu esse no\n "  ;
	}

	std::cout << j  << std::endl;

}


#endif /* GRAPH_HH_ */
