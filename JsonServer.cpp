/*
 * JsonServer.cpp
 *
 *  Created on: 29 de nov de 2016
 *      Author: familia
 */

#include "JsonServer.h"



JsonServer::JsonServer(char* address, int port )
{
	// TODO Auto-generated constructor stub
	// Create a new event handler
	ebase = event_base_new ();;

	// Create a http server using that handler
	server = evhttp_new (ebase);

	this->_port = port;
	_address = address;
}

JsonServer::~JsonServer() {
	// TODO Auto-generated destructor stub
}


int JsonServer::Start()
{
	// Limit serving GET requests
	//evhttp_set_allowed_methods (server, EVHTTP_REQ_GET);

	// Set the callbacks
	evhttp_set_cb (server, "/slice", slice, _slices);

	// Set the callback for anything not recognized
	evhttp_set_gencb (server, notfound, 0);


	// Listen locally on port 32001
	if (evhttp_bind_socket (server, "127.0.0.1", _port) != 0)
	{
		printf("Could not bind to 127.0.0.1:%d \n",_port);
		return 1;
	}
		printf("Running Slice mgmt at port: %i",_port);

	// Start processing queries
	event_base_dispatch(ebase);


}

void JsonServer::Stop()
{

	// Free up stuff
	evhttp_free (server);

	event_base_free (ebase);
}

