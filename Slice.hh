/*
 * Slice.hh
 *
 *  Created on: 28/10/2016
 *      Author: pedro
 */

#ifndef SLICE_HH_
#define SLICE_HH_

#include <string.h>
#include "json.hpp"

using json = nlohmann::json;

class Slice {
private:
	std::string gnc_ip;
	std::string gnc_port;

	std::string json_string;
	json j;

public:
	Slice(std::string json_string, std::string gnc_ip, std::string gnc_port){
		this->gnc_ip = gnc_ip;
		this->gnc_port = gnc_port;

		this->json_string = json_string;
		j = json::parse(json_string);
	}

	std::string getJsonString() {
		return this->json_string;
	}

	std::string getGncIP() {
		return this->gnc_ip;
	}

	std::string getGncPort() {
		return this->gnc_port;
	}
};

#endif /* SLICE_HH_ */
