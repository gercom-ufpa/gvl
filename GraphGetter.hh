#include <boost/graph/adjacency_list.hpp> // for customizable graphs
#include <boost/graph/directed_graph.hpp> // A subclass to provide reasonable arguments to adjacency_list for a typical directed graph
#include <boost/graph/undirected_graph.hpp>// A subclass to provide reasonable arguments to adjacency_list for a typical undirected graph

  // Construct a graph with the vertices container as a set
  typedef boost::adjacency_list<boost::vecS, boost::setS, boost::bidirectionalS> Graph;

  // Graph g; // Create a graph.
  // Graph::vertex_descriptor v0:12:12:sd:ds = boost::add_vertex(g);
  // Graph::vertex_descriptor v1 = boost::add_vertex(g);
  // Graph::vertex_descriptor v2 = boost::add_vertex(g);
  //
  // boost::add_edge(v0, v1, g);
  // boost::add_edge(v1, v2, g);
