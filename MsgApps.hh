#ifndef __MSGAPPS_HH__
#define __MSGAPPS_HH__

#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>

#include "Controller.hh"
#include "Slice.hh"
#include <fluid/of10msg.hh>
#include <fluid/of13msg.hh>
#include "graph.hh"
#include "gvlnet.hh"
#include <restclient-cpp/restclient.h>
#include "json.hpp"


using namespace fluid_msg;
using json = nlohmann::json;


class CBench: public Application {
    virtual void event_callback(ControllerEvent* ev) {
        if (ev->get_type() == EVENT_PACKET_IN) {
            of10::FlowMod fm;
            fm.command(of10::OFPFC_ADD);
            uint8_t* buffer;
            buffer = fm.pack();

            ev->ofconn->send(buffer, fm.length());
            OFMsg::free_buffer(buffer);
        }
    }
};

class MultiLearningSwitch: public BaseLearningSwitch {
public:
	Graph g;
	std::vector<Slice> * slices;

	MultiLearningSwitch(std::vector<Slice> * slices_){
		slices = slices_;
		Slice * s;
		// reading Slices file
		std::string line;
		std::ifstream slices_file ("SlicesConf");
		if (slices_file.is_open()) {
			while (getline(slices_file, line)){
				//std::cout << line << std::endl;
				// create Slicesz
				if (line.find("Slice")!=std::string::npos){
					std::string json_string, gnc_ip, gnc_port;
					getline(slices_file, json_string); // get the next line, which contains the json string with the slice
					getline(slices_file, gnc_ip); // get the next line, which contains the GNC IP with the slice
					getline(slices_file, gnc_port); // get the next line, which contains the GNC Port number with the slice
					s = new Slice(json_string, gnc_ip, gnc_port);
					slices->push_back(*s);
					//std::cout << "slice: \n" << json_string << std::endl;
				}
				//std::cout << "----------------" << std::endl;
			}
			slices_file.close();
		}
		else
			std::cout << "unable to open file" << std::endl;

		// print slices
//		std::cout << "Slices: " << std::endl;
//		for (int i=0; i < slices.size(); i++){
//			std::cout << "Slice " << i << std::endl;
//			std::cout << slices[i].getJsonString() << std::endl;
//			std::cout << slices[i].getGncIP() << std::endl;
//			std::cout << slices[i].getGncPort() << std::endl;
//			std::cout << std::endl;
//		}
//		std::cout << std::endl;

		// for each slice send POST
		//std::cout << "slices->size() = " << slices->size() << std::endl;
		for (int i=0; i < slices->size(); i++){
			// Send POST (slice topology)
			//std::cout << "Send POST (slice topology)" << std::endl;
			std::stringstream gnc_post_string;
			gnc_post_string << slices->at(i).getGncIP() << ":" << slices->at(i).getGncPort() << "/topology";
			//std::cout << "Post string: " << slices->at(i).getJsonString() << std::endl;
			RestClient::Response client_post = RestClient::post(gnc_post_string.str(), "text/json", slices->at(i).getJsonString());
			//std::cout << client_post.body << std::endl;
		}


	}

    virtual void event_callback(ControllerEvent* ev) {
        uint8_t ofversion = ev->ofconn->get_version();

        //std::cout << "Tipo de Evento: " << ev->get_type() << "\n";

        if (ev->get_type() == EVENT_PACKET_IN) {
            //L2TABLE* l2table = get_l2table(ev->ofconn);

        	sw_t * sw = get_sw(ev->ofconn);
            if (sw == NULL) {
                return;
            }
            L2TABLE* l2table = sw->l2t;
            struct ethhdr pkt_mac;
            uint64_t dst = 0, src = 0, src_mac = 0;
			EthAddress mac_ether = EthAddress("00:00:00:00:00:00");

            PacketInEvent* pi = static_cast<PacketInEvent*>(ev);
            void* ofpip;
            void* data;
            uint16_t in_port;
            char mac[6];
            int pkt_type;
            if (ofversion == of10::OFP_VERSION) {
            	//std::cout << " - - - - - - of10::OFP_VERSION " << std::endl;
                of10::PacketIn *ofpi = new of10::PacketIn();
                ofpip = ofpi;
                ofpi->unpack(pi->data);
                data = (uint8_t*) ofpi->data();
                memcpy(((uint8_t*) &dst) + 2, (uint8_t*) ofpi->data(), 6);
                memcpy(((uint8_t*) &src) + 2, (uint8_t*) ofpi->data() + 6, 6);
                in_port = ofpi->in_port();
            }
            else if (ofversion == of13::OFP_VERSION)
            {
            	//std::cout << " - - - - - - of13::OFP_VERSION " << std::endl;

                of13::PacketIn *ofpi = new of13::PacketIn();
                ;
                ofpip = ofpi;
                ofpi->unpack(pi->data);
                data = (uint8_t*) ofpi->data();
                memcpy(((uint8_t*) &dst) + 2, (uint8_t*) ofpi->data(), 6);
                memcpy(((uint8_t*) &src) + 2, (uint8_t*) ofpi->data() + 6, 6);
                if (ofpi->match().in_port() == NULL) {
                    return;
                }
                in_port = ofpi->match().in_port()->value();
				memcpy(&pkt_mac, (struct ethhdr*)ofpi->data(),sizeof(struct ethhdr));
                pkt_type = ntohs(pkt_mac.h_proto);
                src_mac = mac2int(pkt_mac.h_source);
            }

            //if the packet is lldp
            if (pkt_type == LLDP_PROTO_TYPE)
            {
            	struct glldp_packet pkt_lldp;
            	memcpy(&pkt_lldp, (struct glldp_packet *)data,sizeof(struct glldp_packet));

//            	std::cout << "Switch que recebeu o lldp: " << ev->ofconn->get_id() << std::endl;
//				std::cout << "src_mac: " << src_mac <<  " porta send: " << ntohs(pkt_lldp.port) << std::endl;

                // Learn the source
            	// atualiza tabela do switch que recebeu o lldp
                (*l2table)[src_mac].port = in_port;
                (*l2table)[src_mac].host = 0;


                // atualiza a tabela do switch q mandou o lldp
                for(auto const& sw: (l2tables_new))
                {
                	if(sw.first == src_mac)
                	{
                		(*sw.second->l2t)[ev->ofconn->get_id()].port = ntohs(pkt_lldp.port);
                		(*sw.second->l2t)[ev->ofconn->get_id()].host = 0;
                		break;
                	}
                }

//                std::cout<< "Porta : " << (*l2table)[src_mac].port <<  " host : " << (*l2table)[src_mac].host << "\n";
//                std::cout << std::endl;
//                std::cout << "------------------------------------------------------------------" << std::endl;
            }
            //if the packet is arp
            else if (pkt_type == ARP_PROTO_TYPE)
            {
            	//std::cout << "Temos um ARP na porta: " << in_port << "\n";
            	//print_l2tables();

            	int from_host = 1;

            	//define if the packet is from hosts, not from switches
            	for(auto const& ports: *l2table)
            	{
            		if (ports.second.port == in_port)
            		{
            			from_host = ports.second.host;
            			break;
            		}
            	}

            	if (from_host )
            	{

                	struct arp_packet pkt_arp;
                	memcpy(&pkt_arp, (struct arp_packet *)data,sizeof(struct arp_packet));


                	// Learn the source
                	(*l2table)[src_mac].port = in_port;
                	(*l2table)[src_mac].host = 1;
                	for(int i=0;i<4;i++) (*l2table)[src_mac].ip[i] = pkt_arp.sndr_ip_addr[i];


            		if (ntohs(pkt_arp.op) == ARP_PROTO_REQUEST_TYPE)
            		{
            			// Figure out which slice the host that sent the ARP Request belongs.
            			std::string json_string;
            			json j;

						for (Slice n : *slices){
//							std::cout << "Slice -> ";
							//std::cout << n.getJsonString() << '\n';
							mac_ether.set_data(pkt_arp.sndr_hw_addr);
							//std::cout << "mac_ether.to_string() = " << mac_ether.to_string() << std::endl;
							if (n.getJsonString().find(mac_ether.to_string())!=std::string::npos){
//								std::cout << "Found the slice" << std::endl;
								json_string = n.getJsonString();
								j = json::parse(json_string);
							}
						}

						// para cada switch na l2table
						for(auto const& sw: (l2tables_new))
						{
//							std::cout  <<  "Switch : " <<  sw.first <<  std::endl;
							std::string sw_first = std::to_string(sw.second->datapath_id);
//							std::cout << "sw_first: " << sw_first << std::endl;
							for (int i=0; i < j["switches"].size(); i++){ // para cada switch no slice
								std::string sw_dpid = j["switches"][i];
//								std::cout << "\tsw_dpid:" << sw_dpid << std::endl;
								if (sw_first == sw_dpid){ // se o switch pertence ao slice
									for(int i=0; i < j["links"].size(); i++) { // percorre os links do slice
//										std::cout << "\t\tlink[" << i << "]: " << j["links"][i] << std::endl;

										// se o switch é um dos nos desse link então a porta desse link pertence ao slice
										// verifica se switch é o nó de saida
										std::string src_dpid = j["links"][i]["src"]["dpid"];
//										std::cout << "\t\t\tsrc_dpid: " << src_dpid << std::endl;
										if (sw_first == src_dpid) {
											std::string str_port = j["links"][i]["src"]["port"];
//											std::cout << "\t\t\t\tstr_port: " << str_port << std::endl;
											int port = std::stoi(str_port);

											int manda = 1;

											for(auto const& ports: *sw.second->l2t)
											{
												if (ports.second.port == port)
												{
													if ((ports.second.host == 0) || ((port == in_port) && (sw.first == ev->ofconn->get_id())))
													{
														manda = 0;
														break;
													}
												}
											}
//											std::cout << "\t\t\t\tmanda: " << manda << std::endl;
											if (manda) send_arp(&(*sw.second->ofconn), pkt_arp.sndr_hw_addr,pkt_arp.sndr_ip_addr,  pkt_arp.rcpt_ip_addr, port); // send arp
										}
									}
								}
							}
						}
            		}
					else if (ntohs(pkt_arp.op) == ARP_PROTO_REPLAY_TYPE)
					{
						//std::cout << "ARP_PROTO_REPLAY_TYPE" << std::endl;
						for(auto const &sw_replay: l2tables_new)
						{
							auto it_replay = sw_replay.second->l2t->find(mac2int(pkt_arp.targ_hw_addr));

							if (it_replay != l2table->end())
							{ 
								//colocar isso tudo em uma funcao e retirar daqui
								/*
								 * Send Slice to GNC
								 */
								//esta investido pois estou em um arp_replay
								mac_ether.set_data(pkt_mac.h_source);
								std::string src_flow = mac_ether.to_string();

								mac_ether.set_data(pkt_mac.h_dest);
								std::string dst_flow = mac_ether.to_string();


								std::string json_string, gnc_ip, gnc_port;

								// Figure out which slice the host belong
								for (Slice n : *slices){

									if (n.getJsonString().find(dst_flow)!=std::string::npos){
										json_string = n.getJsonString();
										gnc_ip = n.getGncIP();
										gnc_port = n.getGncPort();

									}
								}

				//				Send POST (slice topology)
								//std::cout << "Send POST (slice topology)" << std::endl;
								std::stringstream gnc_post_string;
								gnc_post_string << gnc_ip << ":" << gnc_port << "/topology";
								//std::cout << "Post string: " << gnc_post_string.str() << std::endl;
								RestClient::Response client_post = RestClient::post(gnc_post_string.str(), "text/json", json_string);
								//std::cout << client_post.body << std::endl;

								// Send FLOW_MODs

								// Going flow...
								//std::cout << "\nGoing flow...\n" << std::endl;

								// Send GET (what to do)
								std::stringstream gnc_get_string_going;
								gnc_get_string_going << gnc_ip << ":" << gnc_port << "/topology";


								gnc_get_string_going << "?ori=" << src_flow << "&dst=" << dst_flow;
								//std::cout << "gnc_get_string_going: " << gnc_get_string_going.str() << std::endl;
								RestClient::Response client_get_going = RestClient::get(gnc_get_string_going.str());
								std::string going_path = client_get_going.body;

								// só para testar sem depende do GNC
				//						std::string going_path = "{\"switches\":[\"00:00:00:00:00:25\",\"1\",\"2\",\"00:00:00:00:00:26\"],\"links\":[{\"linkId\":1,\"dst\":{\"port\":\"1\",\"dpid\":\"1\"},\"src\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:25\"}},{\"linkId\":3,\"dst\":{\"port\":\"1\",\"dpid\":\"2\"},\"src\":{\"port\":\"2\",\"dpid\":\"1\"}},{\"linkId\":5,\"dst\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:26\"},\"src\":{\"port\":\"2\",\"dpid\":\"2\"}}]}";

								//std::cout << "Going Path:\n" << going_path << std::endl;

								json json_going = json::parse(going_path); // To access the information easily


								for (int i=1; i < json_going["links"].size(); i++){
									for(auto const& sw: (l2tables_new)) {

										std::string sw_first = std::to_string(sw.second->datapath_id);
										std::string dpid = json_going["links"][i]["src"]["dpidSrc"];
										std::string dport = json_going["links"][i]["src"]["portSrc"];

										if (sw_first == dpid)
										{
											//std::cout << "\t\tFound the Switch!" << std::endl;
											//std::cout << "$$$$$$ dpid: " << dpid << " dport: " << dport << std::endl;
											//if is the destination swich, send the packet stored

											install_flow_mod13(*((of13::PacketIn*) pi), &(*sw.second->ofconn), src, dst, std::stoi(dport), 51,0);
										}
									}
								}


								// Come back flow...
								//std::cout << "\nCome back flow...\n" << std::endl;

								std::stringstream gnc_get_string_come_back;
								gnc_get_string_come_back << gnc_ip << ":" << gnc_port << "/topology";
								gnc_get_string_come_back << "?ori=" << dst_flow << "&dst=" << src_flow;
				//						std::cout << "gnc_get_string_come_back: " << gnc_get_string_come_back.str() << std::endl;
								RestClient::Response client_get_come_back = RestClient::get(gnc_get_string_come_back.str());
								std::string come_back_path = client_get_come_back.body;

								// só para testar sem depende do GNC
								// std::string come_back_path = "{\"switches\":[\"00:00:00:00:00:25\",\"1\",\"2\",\"00:00:00:00:00:26\"],\"links\":[{\"linkId\":6,\"dst\":{\"port\":\"2\",\"dpid\":\"2\"},\"src\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:26\"}},{\"linkId\":4,\"dst\":{\"port\":\"2\",\"dpid\":\"1\"},\"src\":{\"port\":\"1\",\"dpid\":\"2\"}},{\"linkId\":2,\"dst\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:25\"},\"src\":{\"port\":\"1\",\"dpid\":\"1\"}}]}";


								//std::cout << "Come back path:\n" << come_back_path << std::endl;

								json json_come_back = json::parse(come_back_path); // To access the information easily

								for (int i=1; i < json_come_back["links"].size(); i++){
									for(auto const& sw: (l2tables_new)) {

										std::string sw_first = std::to_string(sw.second->datapath_id);
										std::string dpid = json_come_back["links"][i]["src"]["dpidSrc"];
										std::string dport = json_come_back["links"][i]["src"]["portSrc"];

										if (sw_first == dpid)
										{
											//std::cout << "\t\tFound the Switch!" << std::endl;
											//std::cout << "$$$$$$ dpid: " << dpid << " dport: " << dport << std::endl;

											std::string o_port = json_come_back["links"][i]["src"]["portSrc"];
											uint32_t out_port = std::stoi(o_port);

											install_flow_mod13(*((of13::PacketIn*) pi), &(*sw.second->ofconn), dst, src , out_port,51,0);
										}
									}
								}

								EthAddress m = EthAddress(pkt_arp.targ_hw_addr);
								send_arp_replay(sw_replay.second->ofconn, pkt_arp.sndr_hw_addr,pkt_arp.sndr_ip_addr, pkt_arp.rcpt_hw_addr, pkt_arp.rcpt_ip_addr,  it_replay->second.port);

								break;
							}
						}
					}
            	}

            }else if (pkt_type == IP_PROTO_TYPE)
            {
				//std::cout << "IP_PROTO_TYPE" << std::endl;
				/*
				 * Send Slice to GNC
				 */
				std::string json_string, gnc_ip, gnc_port;

				// Figure out which slice the host belong
//				std::cout << " ----- Pacote IP ----- \n" << std::endl;
				for (Slice n : *slices){
//					std::cout << n.getJsonString() << '\n';
					mac_ether.set_data(pkt_mac.h_dest);
//					std::cout << mac_ether.to_string() << '\n';

					if (n.getJsonString().find(mac_ether.to_string())!=std::string::npos){
//						std::cout << "Found the slice" << std::endl;
						json_string = n.getJsonString();
						gnc_ip = n.getGncIP();
						gnc_port = n.getGncPort();

					}
				}

				// Send FLOW_MODs

				// Going flow...
				//std::cout << "\nGoing flow...\n" << std::endl;

				// Send GET (what to do)
				std::stringstream gnc_get_string_going;
				gnc_get_string_going << gnc_ip << ":" << gnc_port << "/topology";
				std::string dest = mac_ether.to_string();
				mac_ether.set_data(pkt_mac.h_source);
				std::string ori = mac_ether.to_string();

				gnc_get_string_going << "?ori=" << ori << "&dst=" << dest;
//				std::cout << "gnc_get_string_going: " << gnc_get_string_going.str() << std::endl;
				RestClient::Response client_get_going = RestClient::get(gnc_get_string_going.str());
				std::string going_path = client_get_going.body;

//				std::cout << "Going Path:\n" << going_path << std::endl;

				json json_going = json::parse(going_path); // To access the information easily

				for (int i=1; i < json_going["links"].size(); i++){
					for(auto const& sw: (l2tables_new)) {

						std::string sw_first = std::to_string(sw.second->datapath_id);
						std::string dpid = json_going["links"][i]["src"]["dpidSrc"];
						std::string dport = json_going["links"][i]["src"]["portSrc"];

						if (sw_first == dpid)
						{
							//std::cout << "\t\tFound the Switch!" << std::endl;
							//std::cout << "$$$$$$ dpid: " << dpid << " dport: " << dport << std::endl;

							install_flow_mod13(*((of13::PacketIn*) pi), &(*sw.second->ofconn), src, dst, std::stoi(dport), 51,0);
						}
					}
				}


				// Come back flow...
				//std::cout << "\nCome back flow...\n" << std::endl;

				std::stringstream gnc_get_string_come_back;
				gnc_get_string_come_back << gnc_ip << ":" << gnc_port << "/topology";
				gnc_get_string_come_back << "?ori=" << dest << "&dst=" << ori;
//						std::cout << "gnc_get_string_come_back: " << gnc_get_string_come_back.str() << std::endl;
				RestClient::Response client_get_come_back = RestClient::get(gnc_get_string_come_back.str());
				std::string come_back_path = client_get_come_back.body;

				// só para testar sem depende do GNC
				// std::string come_back_path = "{\"switches\":[\"00:00:00:00:00:25\",\"1\",\"2\",\"00:00:00:00:00:26\"],\"links\":[{\"linkId\":6,\"dst\":{\"port\":\"2\",\"dpid\":\"2\"},\"src\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:26\"}},{\"linkId\":4,\"dst\":{\"port\":\"2\",\"dpid\":\"1\"},\"src\":{\"port\":\"1\",\"dpid\":\"2\"}},{\"linkId\":2,\"dst\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:25\"},\"src\":{\"port\":\"1\",\"dpid\":\"1\"}}]}";


				//std::cout << "Come back path:\n" << come_back_path << std::endl;

				json json_come_back = json::parse(come_back_path); // To access the information easily

				for (int i=1; i < json_come_back["links"].size(); i++){
					for(auto const& sw: (l2tables_new)) {

						std::string sw_first = std::to_string(sw.second->datapath_id);
						std::string dpid = json_come_back["links"][i]["src"]["dpidSrc"];
						std::string dport = json_come_back["links"][i]["src"]["portSrc"];

						if (sw_first == dpid)
						{
							//std::cout << "\t\tFound the Switch!" << std::endl;
							//std::cout << "$$$$$$ dpid: " << dpid << " dport: " << dport << std::endl;

							std::string o_port = json_come_back["links"][i]["src"]["portSrc"];
							uint32_t out_port = std::stoi(o_port);

							install_flow_mod13(*((of13::PacketIn*) pi), &(*sw.second->ofconn), dst, src, out_port,51,0);
						}
					}
				}
            }

            if (ofversion == of10::OFP_VERSION) {
               //install_flow_mod10(*((of10::PacketIn*) ofpip), ev->ofconn, src, dst, it->second);
                delete (of10::PacketIn*) ofpip;
            }
            else if (ofversion == of13::OFP_VERSION) {
                //install_flow_mod13(*((of13::PacketIn*inet_ntoa) ofpip), ev->ofconn, src,dst, it->second);
                delete (of13::PacketIn*) ofpip;
            }

        }
        else if (ev->get_type() == EVENT_SWITCH_UP) {
        	//std::cout << "EVENT_SWITCH_UP!!!!!\n";
        	//std::cout << "switch:" << ev->ofconn->get_id() << std::endl;
        	SwitchUpEvent* swup_ev = static_cast<SwitchUpEvent*>(ev);
        	of13::FeaturesReply *offr = new of13::FeaturesReply();
        	offr->unpack(swup_ev->data);

        	//of13::PacketIn *ofpi = new of13::PacketIn();
        	//std::cout << "Datapath_id: "<< std::hex  << offr->datapath_id()  << std::endl;

        	//g.addvertex(ev->ofconn->get_id());
            BaseLearningSwitch::event_callback(ev);

        	sw_t * sw = get_sw(ev->ofconn);
            if (sw == NULL) {
                return;
            }
            sw->datapath_id = offr->datapath_id();

            if (ofversion == of13::OFP_VERSION) {
                install_default_flow13(ev->ofconn);

                /*todo
                 *
                 * find the correct max port number and replace the 16
                 * */
                for(int i=0;i<8;i++)
                {
                	//std::cout << "send_lldp\n";
                	send_lldp(ev->ofconn,i);
                }
                //send_arp(ev->ofconn);

            }
//            std::cout << "send_lldp\n";
//            print_l2tables();
//            std::cout << "---------------------------------" << std::endl;
        }
        else if (ev->get_type() == EVENT_PORT_STATUS)
        {
        	//std::cout << "EVENT_PORT_STATUS" << std::endl;
        	PortStatusEvent* ps_ev = static_cast<PortStatusEvent*>(ev);
        	of13::PortStatus *ofps = new of13::PortStatus();
        	ofps->unpack(ps_ev->data);
        	of13::Port port = ofps->desc();

		    //std::cout << "Switch :" << ps_ev->ofconn->get_id() << " Porta: " << port.port_no() << " MAC : " << port.hw_addr().to_string() << "\n"; // mac do switch
        }

        else {
            BaseLearningSwitch::event_callback(ev);
        }
        //std::cout << "############################## Conexão: " << ev->ofconn->get_id() << " Tipo evento: " << ev->get_type() << "\n";


    }

    void print_l2tables(){
    	//codigo para acessar todas as tabelas, sendo cada uma de um switch
		std::cout<< "Tabela de todos os switchs \n";
		for(auto const& sw: (l2tables_new))
		{
			std::cout  <<  "Switch : " <<  sw.first <<  std::endl;
			std::cout  <<  "Datapath_id : " <<  sw.second->datapath_id <<  std::endl;
			for(auto const &entry: (*sw.second->l2t))
			{

				//std::cout  /*<<  "Mac: " << entry.first*/  << " Port: "<<  entry.second.port << std::endl;

				// Pega o MAC e deixar da forma conhecida
				// Convert decimal to hex
				//cont long unsigned int decimal_value = entry.first;
				//std::cout << "entry.first = " << entry.first << "\ndecimal_value = " << decimal_value << std::endl;
				std::stringstream ss;
				//ss<< std::hex << decimal_value;
				ss << std::hex << entry.first;
				std::string hex ( ss.str() );
				//int hex_tam = hex.size();
				//std::cout << "hex.size() = " << hex_tam << std::endl;
				//std::cout << "Depois da conversão\nhex = " << hex << std::endl;

				// Verifica se precisa colocar zeros no começo da string
				if (hex.size() < 12){
					std::stringstream temp;
					for (int i=0; i < 12-hex.size(); i++)
						temp << "0";
					temp << hex;
					//std::cout << "temp = " << temp.str() << std::endl;
					hex = temp.str();
					//std::cout << "hex = " << hex << std::endl;
				}
				//std::cout << "Depois do preenchimento\nhex = " << hex << std::endl;

				// Coloca os dois pontos a cada dois caracteres (só para efeito de visualização)
				std::stringstream temp;
				temp << hex[0];
				temp << hex[1];
				for (int i=2; i < hex.size(); i=i+2){
					temp << ":";
					temp << hex[i];
					temp << hex[i+1];
				}
				//std::cout << "temp = " << temp.str() << std::endl;
				std::string str_mac = temp.str();
				std::cout << "MAC = " << str_mac << std::endl;

				std::cout  /*<<  "Mac: " << entry.first*/  << "Port: "<<  entry.second.port << std::endl;


				// Se for um host pega o IP e deixa da forma conhecida
				std::cout << "Host: " << entry.second.host << std::endl;
				std::stringstream getting_ip;
				if (entry.second.host == 1)
				{
					for(int i=0;i<4;i++)
					{
						//std::cout << "entry.second.ip[i] = " << entry.second.ip[i] << std::endl;

						int q = entry.second.ip[i];
						getting_ip << q;
						if(i < 3)
							getting_ip << ".";
						//std::cout << "q = " << q << std::endl;
						//std::cout << q << ".";
					}
					std::string str_ip (getting_ip.str());
					std::cout  << "IP: " << str_ip << std::endl;
					std::cout << std::endl;
				}
				std::cout << std::endl;

			}
			std::cout  <<  "Acabou a tabela do switch : " <<  sw.first << std::endl;
		}
	std::cout << std::endl;
    }

    void install_flow_mod10(of10::PacketIn &pi, OFConnection* ofconn,
        uint64_t src, uint64_t dst, uint16_t out_port) {
        // Flow mod message
        uint8_t* buffer;
        /* Messages constructors allow to add all 
         values in a row. The fields order follows
         the specification */
        of10::FlowMod fm(pi.xid(),  //xid 
            123, // cookie
            of10::OFPFC_ADD, // command
            5, // idle timeout
            10, // hard timeout
            100, // priority
            pi.buffer_id(), //buffer id
            0, // outport
            0); // flags
        of10::Match m;
        m.dl_src(((uint8_t*) &src) + 2);
        m.dl_dst(((uint8_t*) &dst) + 2);
        fm.match(m);
        of10::OutputAction act(out_port, 1024);
        fm.add_action(act);
        buffer = fm.pack();
        ofconn->send(buffer, fm.length());
        OFMsg::free_buffer(buffer);
    }

    void flood10(of10::PacketIn &pi, OFConnection* ofconn) {
        uint8_t* buf;
        of10::PacketOut po(pi.xid(), pi.buffer_id(), pi.in_port());
        /*Add Packet in data if the packet was not buffered*/
        if (pi.buffer_id() == -1) {
            po.data(pi.data(), pi.data_len());
        }
        of10::OutputAction act(of10::OFPP_FLOOD, 1024);
        po.add_action(act);
        buf = po.pack();
        ofconn->send(buf, po.length());
        OFMsg::free_buffer(buf);
    }

    void install_default_flow13(OFConnection* ofconn) {
        uint8_t* buffer;
        of13::FlowMod fm(42, 0, 0xffffffffffffffff, 0, of13::OFPFC_ADD, 0, 0, 0,
            0xffffffff, 0, 0, 0);
        of13::OutputAction *act = new of13::OutputAction(of13::OFPP_CONTROLLER,
            of13::OFPCML_NO_BUFFER);
        of13::ApplyActions *inst = new of13::ApplyActions();
        inst->add_action(act);
        fm.add_instruction(inst);
        buffer = fm.pack();
        ofconn->send(buffer, fm.length());
        OFMsg::free_buffer(buffer);
    }

    void install_flow_mod13(of13::PacketIn &pi, OFConnection* ofconn,
        uint64_t src, uint64_t dst, uint32_t out_port, uint16_t vlan_id, int remove_vlan) {
        // Flow mod message
        uint8_t* buffer;
        /*You can also set the message field using
         class methods which have the same names from
         the field present on the specification*/
        of13::FlowMod fm;
        //fm.xid(pi.xid());
        fm.xid(0);
        fm.cookie(123);
        fm.cookie_mask(0xffffffffffffffff);
        fm.table_id(0);
        fm.command(of13::OFPFC_ADD);
        fm.idle_timeout(60);
        fm.hard_timeout(200);
        fm.priority(100);
        //fm.buffer_id(pi.buffer_id());
        fm.out_port(0);
        fm.out_group(0);
        fm.flags(0);
        of13::EthSrc fsrc(((uint8_t*) &src) + 2);
        of13::EthDst fdst(((uint8_t*) &dst) + 2);
        //of13::InPort finport(in_port);
        fm.add_oxm_field(fsrc);
        fm.add_oxm_field(fdst);
        //fm.add_oxm_field(finport)
        of13::ApplyActions inst;

        /*
        if (vlan_id)
        {
            of13::VLANVid fvlan(vlan_id);
            fm.add_oxm_field(fvlan);

            if(remove_vlan)
        	{
            	of13::PopVLANAction pvlan;
            	inst.add_action(pvlan);
        	}

        }
        else
        {
        	of13::VLANVid fvlan(vlan_id);
        	of13::OXMTLV ox;
        	ox = fvlan;
            of13::SetFieldAction setfield(&ox);
            of13::PushVLANAction actvlan(0x8100);
            inst.add_action(actvlan);
            inst.add_action(setfield);
        }
*/

        of13::OutputAction act(out_port, 1024);
        inst.add_action(act);
        fm.add_instruction(inst);
        buffer = fm.pack();
        ofconn->send(buffer, fm.length());
        OFMsg::free_buffer(buffer);
        of13::Match m;
        of13::MultipartRequestFlow rf(2, 0x0, 0, of13::OFPP_ANY, of13::OFPG_ANY,
            0x0, 0x0, m);
        buffer = rf.pack();
        ofconn->send(buffer, rf.length());
        OFMsg::free_buffer(buffer);
    }

    void flood13(of13::PacketIn &pi, OFConnection* ofconn, uint32_t in_port) {
        uint8_t* buf;
        of13::PacketOut po(pi.xid(), pi.buffer_id(), in_port);
        /*Add Packet in data if the packet was not buffered*/
        if (pi.buffer_id() == -1) {
            po.data(pi.data(), pi.data_len());
        }
        of13::OutputAction act(of13::OFPP_FLOOD, 1024); // = new of13::OutputAction();
        po.add_action(act);
        buf = po.pack();
        ofconn->send(buf, po.length());
        OFMsg::free_buffer(buf);
    }

    void send_lldp(OFConnection* ofconn, int in_port) {
            uint8_t* buf;
            int id = ofconn->get_id();
            struct ethhdr mac_pkt;
            mac_pkt.h_proto = htons(ARP_PROTO_TYPE);
            EthAddress mac = EthAddress("01:80:c2:00:00:0e");


            struct glldp_packet lldp_pkt ;

            lldp_pkt.h_proto = htons(LLDP_PROTO_TYPE);
            memcpy(&lldp_pkt.h_dest, mac.get_data(),6);

            //lldp_pkt.h_source[0] = 0;
            //lldp_pkt.h_source[1] = 0;
            //lldp_pkt.h_source[2] = 0;
            //lldp_pkt.h_source[3] = 0;
            //lldp_pkt.h_source[4] = (unsigned char)(id >> 8);
            //lldp_pkt.h_source[5] = id & 0xff;
            int2mac(id,lldp_pkt.h_source);

            lldp_pkt.port = hton16(in_port);
            //memcpy(&lldp_pkt.h_source, &id,6);
            mac.set_data(lldp_pkt.h_source);


            //std::cout << "send_lldp: " << "id: " << id << " mac: " << mac.to_string() << std::endl;

            of13::PacketOut po = of13::PacketOut();
            //set the buffer_od to none, the data goes inside the .data buff
            po.buffer_id(0xffffffff);
            po.data(&lldp_pkt, sizeof(lldp_pkt));

            of13::OutputAction act(of13::OFPP_IN_PORT, 1024); // = new of13::OutputAction();
            po.in_port(in_port);


            po.add_action(act);
            buf = po.pack();
            ofconn->send(buf, po.length());
            OFMsg::free_buffer(buf);
        }

    void send_arp(OFConnection* ofconn,u_char * src_mac, u_char *src_ip, u_char *dst_ip, int in_port) {
            uint8_t* buf;
            int id = ofconn->get_id() ;
            EthAddress mac = EthAddress("ff:ff:ff:ff:ff:ff");
            struct ethhdr mac_pkt;
            struct arp_packet pkt;

            //creating mac header
            //for(int i=0;i<6;i++) mac_pkt.h_source[i] = src_mac[i];
            //memcpy(&mac_pkt.h_dest, mac.get_data(),6);
            //mac_pkt.h_proto = htons(ARP_PROTO_TYPE);


            //ether pkt
            for(int i=0;i<6;i++) pkt.src_hw_addr[i] = src_mac[i];
            memcpy(&pkt.targ_hw_addr, mac.get_data(),6);
            pkt.frame_type     = htons(ARP_FRAME_TYPE);

            //arp packet
            pkt.hw_type        = htons(ETHER_HW_TYPE);
            pkt.prot_type      = htons(IP_PROTO_TYPE);
            pkt.hw_addr_size   = ETH_HW_ADDR_LEN;
            pkt.prot_addr_size = IP_ADDR_LEN;
            pkt.op             = htons(OP_ARP_REQUEST);

            for(int i=0;i<6;i++) pkt.sndr_hw_addr[i] = src_mac[i];
            for(int i=0;i<4;i++) pkt.sndr_ip_addr[i] = src_ip[i];
            for(int i=0;i<4;i++) pkt.rcpt_hw_addr[i] = 0;
            for(int i=0;i<4;i++) pkt.rcpt_ip_addr[i] = dst_ip[i];


            of13::PacketOut po = of13::PacketOut();
            //set the buffer_od to none, the data goes inside the .data buff
            po.buffer_id(0xffffffff);
            po.data(&pkt, sizeof(struct arp_packet));

            of13::OutputAction act(of13::OFPP_IN_PORT, 1024); // = new of13::OutputAction();
            po.in_port(in_port);
            po.add_action(act);
            buf = po.pack();
            ofconn->send(buf, po.length());
            OFMsg::free_buffer(buf);
        }

    void send_arp_replay(OFConnection* ofconn,u_char * src_mac, u_char *src_ip, u_char *dst_mac, u_char *dst_ip, int in_port) {
            uint8_t* buf;
            int id = ofconn->get_id() ;
            EthAddress mac = EthAddress("ff:ff:ff:ff:ff:ff");
            struct ethhdr mac_pkt;
            struct arp_packet pkt;

            pkt.frame_type     = htons(ARP_FRAME_TYPE);
            for(int i=0;i<6;i++) pkt.src_hw_addr[i] = src_mac[i];
            for(int i=0;i<6;i++) pkt.targ_hw_addr[i] = dst_mac[i];

            pkt.hw_type        = htons(ETHER_HW_TYPE);
            pkt.prot_type      = htons(IP_PROTO_TYPE);
            pkt.hw_addr_size   = ETH_HW_ADDR_LEN;
            pkt.prot_addr_size = IP_ADDR_LEN;
            pkt.op             = htons(OP_ARP_REPLAY);

            for(int i=0;i<6;i++) pkt.sndr_hw_addr[i] = src_mac[i];
            for(int i=0;i<4;i++) pkt.sndr_ip_addr[i] = src_ip[i];
            for(int i=0;i<6;i++) pkt.rcpt_hw_addr[i] = dst_mac[i];
            for(int i=0;i<4;i++) pkt.rcpt_ip_addr[i] = dst_ip[i];


            //buf = (uint8_t*) malloc(sizeof(struct ethhdr)+sizeof(struct arp_packet));
            //memcpy(buf,(uint8_t*)&mac_pkt,sizeof(struct ethhdr));
            //memcpy(buf+sizeof(struct ethhdr),(uint8_t*)&pkt,sizeof(struct arp_packet));


            //std::cout << "Tamanho do MAC : " <<  sizeof(struct ethhdr) << " Tamanho do ARP:  " << sizeof(struct arp_packet) << std::endl;

            of13::PacketOut po = of13::PacketOut();
            //set the buffer_od to none, the data goes inside the .data buff
            po.buffer_id(0xffffffff);
            po.data(&pkt, sizeof(struct arp_packet));


            of13::OutputAction act(of13::OFPP_IN_PORT, 1024); // = new of13::OutputAction();
            po.in_port(in_port);

            po.add_action(act);
            buf = po.pack();

            ofconn->send(buf, po.length());

            OFMsg::free_buffer(buf);
        }

    std::string int2mac_string(u_int64_t mac){
		// Pega o MAC e deixar da forma conhecida
		// Convert decimal to hex
		//cont long unsigned int decimal_value = entry.first;
		//std::cout << "entry.first = " << entry.first << "\ndecimal_value = " << decimal_value << std::endl;
		std::stringstream ss;
		//ss<< std::hex << decimal_value;
		ss << std::hex << mac;
		std::string hex ( ss.str() );
		//int hex_tam = hex.size();
		//std::cout << "hex.size() = " << hex_tam << std::endl;
		//std::cout << "Depois da conversão\nhex = " << hex << std::endl;

		// Verifica se precisa colocar zeros no começo da string
		if (hex.size() < 12){
			std::stringstream temp;
			for (int i=0; i < 12-hex.size(); i++)
				temp << "0";
			temp << hex;
			//std::cout << "temp = " << temp.str() << std::endl;
			hex = temp.str();
			//std::cout << "hex = " << hex << std::endl;
		}
		//std::cout << "Depois do preenchimento\nhex = " << hex << std::endl;

		// Coloca os dois pontos a cada dois caracteres (só para efeito de visualização)
		std::stringstream temp;
		temp << hex[0];
		temp << hex[1];
		for (int i=2; i < hex.size(); i=i+2){
			temp << ":";
			temp << hex[i];
			temp << hex[i+1];
		}
		//std::cout << "temp = " << temp.str() << std::endl;
		std::string str_mac = temp.str();
		//std::cout << "MAC = " << str_mac << std::endl;
		return str_mac;
    }

};

class Nvl: public Application {
    virtual void event_callback(ControllerEvent* ev) {
        if (ev->get_type() == EVENT_PACKET_IN) {
            of10::FlowMod fm;
            fm.command(of10::OFPFC_ADD);
            uint8_t* buffer;
            buffer = fm.pack();
            ev->ofconn->send(buffer, fm.length());
            OFMsg::free_buffer(buffer);
        }
    }
};
#endif
