from flask import Flask, jsonify, abort, make_response, request, url_for
from py2neo import Graph, authenticate, neo4j, cypher
import json,sys

#curl --user neo4j:testeGET -i -H accept:application/json -H content-type:application/json (p:Person {name:{name},born:{born}}) RETURN p","parameters":{"name":"Keanu Reeves","born":1964}}]}'
#sudo docker run -t -i testeGET:gnc /bin/bash


app = Flask(__name__)

'''
POST
'''
@app.route('/topology', methods = ['POST'])

def cria_topo():
    #print request.json
    authenticate("localhost:7474", "neo4j", "teste")
    graph = Graph()
    a= request.data
    #a=json.dumps(request.data)
    #print a
    #a = request.data
    jsondata = json.loads(a)
    #print (jsondata)
    

   # jsondata = request.json
   # print jsondata
    
    
   #     return 'OK', 200
   # else:
   #     return "Unsupported Media Type", 415
        
        #jsondata = request.form['jsondata']
        #print jsondata
        #data = json.loads(jsondata)
    #return 'OK', 200
   # print data
    query = """
    WITH {comando} AS document
    UNWIND document.links AS link
    MERGE (s:Switch {dpid: link.src.dpid})
    MERGE (d:Switch {dpid: link.dst.dpid})
    MERGE (s) - [:CONNECTED {portSrc: link.src.port, dpidSrc:link.src.dpid, dpidDst:link.dst.dpid, portDst: link.dst.port, linkID: link.linkId}] - (d)
    """
    #print "dfsdfh"
    print (graph.cypher.execute(query, comando = jsondata))
    return "ok",200

'''
GET
'''

@app.route('/topology', methods = ['GET'])


def caminhoMin():
    print( "sdhkdfhskdfjh")
    valor=request.data
    ori = request.args.get('ori')
    print (ori)
    dst = request.args.get('dst')
    graph = Graph()
    authenticate("localhost:7474", "neo4j", "teste")
    caminho = graph.cypher.execute("""
    MATCH (p1:Switch {dpid: {ori}}), (p2:Switch {dpid: {dst}}),
      path = shortestpath((p1)-[:CONNECTED*]->(p2))
    RETURN path""",{"ori": ori, "dst": dst},)
    final=trataCaminhoMin(caminho)
    return final,200
    
    
def trataCaminhoMin(caminho):   
     
    caminho = str(caminho[0][0])
    caminho = str(caminho.split("\""))
    caminho = caminho.split(",")
    lista=[]
    totalLista=0
    for i in caminho:
        if (i !="['(:Switch {dpid:'"):
            if (i !=" '})-[:CONNECTED {dpidDst:'"):
                if (i !="dpidSrc:'"):
                    if (i !=" '"):
                        if (i !=" '"):
                            if (i !="portSrc:'"):
                                if (i !="portDst:'"):
                                    if (i !=" '}]->(:Switch {dpid:'"):      
                                        lista.append(i)
    lista.pop()
    final=escreveCaminhoMin(lista)
    return final
                                        
def escreveCaminhoMin(lista):
    dpid=[]
    dpidDst=[]
    dpidSrc=[]
    linkId=[]
    portDst=[]
    portSrc=[]
    
    
    dpid.append(lista[0]) 
    lista.pop(0)
    for i in lista:
        
        dpidDst.append(lista[0]) 
        lista.pop(0)
        dpidSrc.append(lista[0]) 
        lista.pop(0)
        cut=lista[0]
        linkId.append(cut[7:]) 
        lista.pop(0)
        portDst.append(lista[0]) 
        lista.pop(0)
        portSrc.append(lista[0]) 
        lista.pop(0)
        dpid.append(lista[0]) 
        lista.pop(0)
    

    sw=  '{"switches":['
    cont=0
    virgula= False
    for i in dpid:
        if (virgula==True): sw=sw+ ","
        sw= sw+ dpid[cont].replace("'", "\"")
        cont = cont +1
        virgula= True
    sw=sw+ "],"
                   
    cont=0
    lk=""
    virgula= False
    lk=lk+ '"links":['
    for i in dpidDst: # quantidades iguais para todos os elementos do for dpidDst, dpidSrc, portDst, portSrc
        if (virgula==True): lk=lk+ ","
        lk=lk+'{"linkId":'+ linkId[cont].replace("'", "\"")+',"dst":{ "port":'+portDst[cont].replace("'", "\"")+',"dpid":'+dpidDst[cont].replace("'", "\"")+' }, "src": {"port":'+ portSrc[cont].replace("'", "\"")+', "dpid":'+ dpidSrc[cont].replace("'", "\"")+'}}'
        cont= cont+1
        virgula= True
    lk=lk+ "]}"
    final = sw+lk
    return final


if __name__ == '__main__':
    if (len(sys.argv) != 2): 
        print(str(sys.argv))
        print("Use: "+ str(sys.argv[0])+ " portNumber\n" )	
    else :
        app.run(port=int(sys.argv[1]), debug = True)
