from SOAPpy import SOAPProxy
from py2neo import Graph, authenticate, neo4j, cypher
import json
import re

def grafo():
    query = """
    WITH {json} AS document
    UNWIND document.links AS link
    MERGE (s:Switch {dpid: link.src.dpid})
    MERGE (d:Switch {dpid: link.dst.dpid})
    MERGE (s) - [:CONNECTED {portSrc: link.src.port, dpidSrc:link.src.dpid, dpidDst:link.dst.dpid, portDst: link.dst.port, linkID: link.linkId}] - (d)
    """
    print graph.cypher.execute(query, json = json)
    return



def caminhoMin(ori, dst):
    
    caminho = graph.cypher.execute("""
    MATCH (p1:Switch {dpid: {ori}}), (p2:Switch {dpid: {dst}}),
      path = shortestpath((p1)-[:CONNECTED*]->(p2))
    RETURN path""",{"ori": ori, "dst": dst},)
    
    caminho = str(caminho[0][0])
    caminho = str(caminho.split("\""))
    caminho = caminho.split(",")
    lista=[]
    totalLista=0
    for i in caminho:
        if (i !="['(:Switch {dpid:'"):
            if (i !=" '})-[:CONNECTED {dpidDst:'"):
                if (i !="dpidSrc:'"):
                    if (i !=" '"):
                        if (i !=" '"):
                            if (i !="portSrc:'"):
                                if (i !="portDst:'"):
                                    if (i !=" '}]->(:Switch {dpid:'"):      
                                        lista.append(i)
                                    
    lista.pop()
    dpid=[]
    dpidDst=[]
    dpidSrc=[]
    linkId=[]
    portDst=[]
    portSrc=[]
    
    
    dpid.append(lista[0]) 
    lista.pop(0)
    for i in lista:
        
        dpidDst.append(lista[0]) 
        lista.pop(0)
        dpidSrc.append(lista[0]) 
        lista.pop(0)
        cut=lista[0]
        linkId.append(cut[7:]) 
        lista.pop(0)
        portDst.append(lista[0]) 
        lista.pop(0)
        portSrc.append(lista[0]) 
        lista.pop(0)
        dpid.append(lista[0]) 
        lista.pop(0)
    
    
    sw=  '{"switches":['
    cont=0
    virgula= False
    for i in dpid:
        if (virgula==True): sw=sw+ ","
        sw= sw+ dpid[cont].replace("'", "\"")
        cont = cont +1
        virgula= True
    sw=sw+ "],"
                   
    cont=0
    lk=""
    virgula= False
    lk=lk+ '"links":['
    for i in dpidDst: # quantidades iguais para todos os elementos do for dpidDst, dpidSrc, portDst, portSrc
        if (virgula==True): lk=lk+ ","
        lk=lk+'{"linkId":'+ linkId[cont].replace("'", "\"")+',"dst":{ "port":'+portDst[cont].replace("'", "\"")+',"dpid":'+dpidDst[cont].replace("'", "\"")+' }, "src": {"port":'+ portSrc[cont].replace("'", "\"")+', "dpid":'+ dpidSrc[cont].replace("'", "\"")+'}}'
        cont= cont+1
        virgula= True
    lk=lk+ "]}"
    json = sw+lk
    print json
    return


        

server = SOAPProxy('http://localhost:8081/')
json = json.loads(server.topologia())


a="00:00:00:00:00:00:00:02"
b ="00:00:00:00:00:00:00:07"


#graph_db = neo4j.GraphDatabaseService('http://localhost:7474/db/data/')
#session = cypher.Session('http://localhost:7474')


authenticate("localhost:7474", "neo4j", "teste")
graph = Graph()

grafo()
caminho = caminhoMin(a,b)



