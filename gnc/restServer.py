from flask import Flask, jsonify, abort, make_response, request, url_for
from py2neo import Graph, authenticate, neo4j, cypher
import json


app = Flask(__name__)

'''
POST implementation: creation of new task
'''
@app.route('/todo/api/v1.0/tasks', methods = ['POST'])

def cria_topo():
    authenticate("localhost:7474", "neo4j", "teste")
    graph = Graph()
    json = request.json
    query = """
    WITH {json} AS document
    UNWIND document.links AS link
    MERGE (s:Switch {dpid: link.src.dpid})
    MERGE (d:Switch {dpid: link.dst.dpid})
    MERGE (s) - [:CONNECTED {portSrc: link.src.port, dpidSrc:link.src.dpid, dpidDst:link.dst.dpid, portDst: link.dst.port, linkID: link.linkId}] - (d)
    """
    print graph.cypher.execute(query, json = json)
    return jsonify({ 'arquivo' : json }),201


@app.route('/todo/api/v1.0/tasks', methods = ['GET'])
def caminho_min():
    authenticate("localhost:7474", "neo4j", "teste")
    graph = Graph()
    ori = request.args.get('origem')
    dst = request.args.get('destino')
    caminho = graph.cypher.execute("""
    MATCH (p1:Switch {dpid: {ori}}), (p2:Switch {dpid: {dst}}),
      path = shortestpath((p1)-[:CONNECTED*]->(p2))
    RETURN path""",{"ori": ori, "dst": dst},)
    caminho = str(caminho[0][0])
    caminho = str(caminho.split("\""))
    caminho = caminho.split(",")
    lista=[]
    totalLista=0
    for i in caminho:
        if (i !="['(:Switch {dpid:'"):
            if (i !=" '})-[:CONNECTED {dpidDst:'"):
                if (i !="dpidSrc:'"):
                    if (i !=" '"):
                        if (i !=" '"):
                            if (i !="portSrc:'"):
                                if (i !="portDst:'"):
                                    if (i !=" '}]->(:Switch {dpid:'"):      
                                        lista.append(i)
                                    
    lista.pop()
    dpid=[]
    dpidDst=[]
    dpidSrc=[]
    linkId=[]
    portDst=[]
    portSrc=[]
    
    
    dpid.append(lista[0]) 
    lista.pop(0)
    for i in lista:
        
        dpidDst.append(lista[0]) 
        lista.pop(0)
        dpidSrc.append(lista[0]) 
        lista.pop(0)
        cut=lista[0]
        linkId.append(cut[7:]) 
        lista.pop(0)
        portDst.append(lista[0]) 
        lista.pop(0)
        portSrc.append(lista[0]) 
        lista.pop(0)
        dpid.append(lista[0]) 
        lista.pop(0)
    
    
    sw=  '{"switches":['
    cont=0
    virgula= False
    for i in dpid:
        if (virgula==True): sw=sw+ ","
        sw= sw+ dpid[cont].replace("'", "\"")
        cont = cont +1
        virgula= True
    sw=sw+ "],"
                   
    cont=0
    lk=""
    virgula= False
    lk=lk+ '"links":['
    for i in dpidDst: # quantidades iguais para todos os elementos do for dpidDst, dpidSrc, portDst, portSrc
        if (virgula==True): lk=lk+ ","
        lk=lk+'{"linkId":'+ linkId[cont].replace("'", "\"")+',"dst":{ "port":'+portDst[cont].replace("'", "\"")+',"dpid":'+dpidDst[cont].replace("'", "\"")+' }, "src": {"port":'+ portSrc[cont].replace("'", "\"")+', "dpid":'+ dpidSrc[cont].replace("'", "\"")+'}}'
        cont= cont+1
        virgula= True
    lk=lk+ "]}"
    json = sw+lk
    return jsonify({ 'arquivo' : json }),201


if __name__ == '__main__':
    app.run(debug = True)