import requests
import simplejson as sjson
from lxml.html.builder import HEAD


data={
    "switches":[
        "00:00:00:00:00:00:00:05",
        "00:00:00:00:00:00:00:04",
        "00:00:00:00:00:00:00:03"
    ],
    "links":[
        {
            "linkId":14.0,
            "dst":{
                "port":"1",
                "dpid":"00:00:00:00:00:00:00:05"
            },
            "src":{
                "port":"2",
                "dpid":"00:00:00:00:00:00:00:04"
            }
        },
        {
            "linkId":10.0,
            "dst":{
                "port":"2",
                "dpid":"00:00:00:00:00:00:00:03"
            },
            "src":{
                "port":"1",
                "dpid":"00:00:00:00:00:00:00:04"
            }
        },
        {
            "linkId":7.0,
            "dst":{
                "port":"1",
                "dpid":"00:00:00:00:00:00:00:04"
            },
            "src":{
                "port":"2",
                "dpid":"00:00:00:00:00:00:00:03"
            }
        },
        {
            "linkId":1.0,
            "dst":{
                "port":"2",
                "dpid":"00:00:00:00:00:00:00:04"
            },
            "src":{
                "port":"1",
                "dpid":"00:00:00:00:00:00:00:05"
            }
        }
    ]
}
url = "http://localhost:5000/topology"
headers = {'Content-type': 'application/json'}
r = requests.post(url,data=sjson.dumps(data), headers=headers)
print r.content