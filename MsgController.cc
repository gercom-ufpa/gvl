#include <signal.h>
#include <iostream>
#include <string>

#include "MsgApps.hh"
#include "NvlSocket.hh"
//#include "GncClient.hh"

#include "JsonServer.cpp"

std::vector<Slice> slices_global;

Controller ctrl("0.0.0.0", 6634, 2);
JsonServer mgmt("0.0.0.0", 8080);

using namespace std;

NvlSocket nvlsckt;

//typedef SimpleWeb::Client<SimpleWeb::HTTP> HttpClient;

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Choose an application to run (\"l2\" or \"cbench\"):\n");
        printf("  ./msg_controller l2|cbench\n");
        return 0;
    }

    MultiLearningSwitch l2(&slices_global);
    CBench cbench;
    Nvl nvl;

    
    if (!strcmp(argv[1], "l2")) {
        ctrl.register_for_event(&l2, EVENT_PACKET_IN);
        ctrl.register_for_event(&l2, EVENT_PORT_STATUS);
        //ctrl.register_for_event(&arp, EVENT_PACKET_IN);
        ctrl.register_for_event(&l2, EVENT_SWITCH_UP);
        ctrl.register_for_event(&l2, EVENT_SWITCH_DOWN);
        //ctrl.register_for_event(&arp, EVENT_SWITCH_UP);
        //ctrl.register_for_event(&arp, EVENT_SWITCH_DOWN);

        printf("Teste l2 application (MultiLearningSwitch) started\n");
    }
    else if (!strcmp(argv[1], "cbench")) {
        ctrl.register_for_event(&cbench, EVENT_PACKET_IN);
        printf("cbench (CBench) application started\n");
    }
    else if (!strcmp(argv[1], "nvl")) {
        
        // ToDo Connect to GNC
        // Connect()
        //nvlsckt.Start(argc, argv);
        // GNC REST Client
	// get a connection object
	//RestClient::Connection* conn = new RestClient::Connection("localhost:5000");
       // HttpClient client("localhost:5000");
        //POST /topology
         string json_string="{\"switches\":[\"00:00:00:00:00:00:00:05\",\"00:00:00:00:00:00:00:04\",\"00:00:00:00:00:00:00:03\"],\"links\":[{\"linkId\":14.0,\"dst\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:00:00:05\"},\"src\":{\"port\":\"2\",\"dpid\":\"00:00:00:00:00:00:00:04\"}},{\"linkId\":10.0,\"dst\":{\"port\":\"2\",\"dpid\":\"00:00:00:00:00:00:00:03\"},\"src\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:00:00:04\"}},{\"linkId\":7.0,\"dst\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:00:00:04\"},\"src\":{\"port\":\"2\",\"dpid\":\"00:00:00:00:00:00:00:03\"}},{\"linkId\":1.0,\"dst\":{\"port\":\"2\",\"dpid\":\"00:00:00:00:00:00:00:04\"},\"src\":{\"port\":\"1\",\"dpid\":\"00:00:00:00:00:00:00:05\"}}]}";
        //auto r2=client.request("POST", "/topology", json_string);
        //cout << r2->content.rdbuf() << endl;

RestClient::Response r2 = RestClient::post("localhost:5000/topology", "text/json", json_string);
RestClient::Response r = RestClient::get("localhost:5000/topology?ori=00:00:00:00:00:00:00:01&dst=00:00:00:00:00:00:00:02");
cout << r.body << endl;
//cout << r.body << endl;
//cout << r.body << endl;


//RestClient::response r = RestClient::put("http://url.com/put", "text/json", "{\"foo\": \"bla\"}");
//RestClient::response r = RestClient::del("http://url.com/delete");



      	// GET /topology
        //auto r1=client.request("GET", "/topology?ori=00:00:00:00:00:00:00:03&dst=00:00:00:00:00:00:00:04");
        //cout << r1->content.rdbuf() << endl;



        //RestClient::init();
        // Close()
        ctrl.register_for_event(&nvl, EVENT_PACKET_IN);
        printf("nvl application started\n");
    }
    else {
        printf("Invalid application. Must be either \"l2\", \"cbench\" or \"nvl\".\n");
        return 0;
    }

    ctrl.start();
    mgmt.setSlices(&slices_global);
    mgmt.Start();
    wait_for_sigint();
    ctrl.stop();
    mgmt.Stop();

    return 0;
}
