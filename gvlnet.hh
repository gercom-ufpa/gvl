/*
 * gvlnet.hh
 *
 *  Created on: 15 de abr de 2016
 *      Author: billy
 */
#include <linux/if_ether.h>

#ifndef GVLNET_HH_
#define GVLNET_HH_

#define ETH_HW_ADDR_LEN 6
#define IP_ADDR_LEN     4
#define ARP_FRAME_TYPE  0x0806
#define ETHER_HW_TYPE   1
#define IP_PROTO_TYPE   0x0800
#define OP_ARP_REQUEST  1
#define OP_ARP_REPLAY  2

#define LLDP_PROTO_TYPE 0x88ff
#define ARP_PROTO_TYPE 0x0806
#define IP_PROTO_TYPE 0x0800
#define ARP_PROTO_REQUEST_TYPE 1
#define ARP_PROTO_REPLAY_TYPE 2

struct arp_packet {
  u_char  targ_hw_addr[ETH_HW_ADDR_LEN];
  u_char  src_hw_addr[ETH_HW_ADDR_LEN];
  u_short frame_type;
  u_short hw_type;
  u_short prot_type;
  u_char  hw_addr_size;
  u_char  prot_addr_size;
  u_short op;
  u_char  sndr_hw_addr[ETH_HW_ADDR_LEN];
  u_char  sndr_ip_addr[IP_ADDR_LEN];
  u_char  rcpt_hw_addr[ETH_HW_ADDR_LEN];
  u_char  rcpt_ip_addr[IP_ADDR_LEN];
  u_char  padding[18];
};

struct glldp_packet {
  u_char  h_dest[ETH_HW_ADDR_LEN];
  u_char  h_source[ETH_HW_ADDR_LEN];
  u_short h_proto;
  int port;
};

class ArpPacket {
public:
	arp_packet data;
private:

};

u_int64_t mac2int(u_int8_t *mac)
{
	return uint64_t(mac[0]) << 40 |
	                        uint64_t(mac[1]) << 32 |
	                        uint64_t(mac[2]) << 24 |
	                        uint64_t(mac[3]) << 16 |
	                        uint64_t(mac[4]) << 8 |
	                        uint64_t(mac[5]);

}

void int2mac(int id, u_char mac[])
{
	mac[0] = 0;
    mac[1] = 0;
    mac[2] = 0;
    mac[3] = 0;
    mac[4] = (unsigned char)(id >> 8);
    mac[5] = id & 0xff;
}



#endif /* GVLNET_HH_ */
