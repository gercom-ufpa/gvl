Iniciando projeto do GVL pelo grupo

- Formato do grafo em JSON:
{
  "nodes": [
    1, 2, 3, 4, 5, 6
  ],
  "vertices": {
    "1": [1,2],
    "2": [1,5],
    "3": [2,3],
    "4": [2,5],
    "5": [3,4],
    "6": [4,5],
    "7": [4,6]
  }
}

Dependencies GVL 
libfluid - https://github.com/OpenNetworkingFoundation/libfluid
restClient  - https://github.com/mrtazz/restclient-cpp

Dependencies GNC
libflask
py2neo v2 - https://github.com/nigelsmall/py2neo/releases/tag/py2neo-2.0.8